ifeq ($(strip $(shell which pkg-config > /dev/null || echo NONE)),NONE)
  $(error This makefile requires pkg-config)
endif

ifeq ($(strip $(shell which $(CC) > /dev/null || echo NONE)),NONE)
  $(info lua-scrypt: Hmm, CC make variable contains nonsense.  Overriding to gcc.)
  CC := gcc
endif

NAKED_LUA_VER := $(shell (pkg-config --exists lua && \
                          pkg-config --modversion lua || \
                          pkg-config --variable V lua) | cut -d. -f1-2)

# find appropriate Lua interpreter
LUA_REPL := $(shell (which lua5.1 > /dev/null 2>&1 && echo lua5.1) || \
                    (which lua-5.1 > /dev/null 2>&1 && echo lua-5.1) || \
                    (which lua51  > /dev/null 2>&1 && echo lua51) || \
                    (lua -e"print(_VERSION)" | grep -q 5.1 && echo lua) || \
                    (which lua-5.2 > /dev/null 2>&1 && echo lua-5.2) || \
                    (which lua52 > /dev/null 2>&1 && echo lua52))

# find appropriate Lua libraries
define find_lua_libs
LUA$(1)_PKG := $$(shell (pkg-config --exists lua$(2) && echo lua$(2)) || \
                     (pkg-config --exists lua-$(2) && echo lua-$(2)) || \
                     (pkg-config --exists lua$(1) && echo lua$(1)) || \
                     (echo "NONE"))

LUA$(1)_PKG := $$(strip $$(LUA$(1)_PKG))

ifeq ($$(LUA$(1)_PKG),NONE)
  ifeq ($$(NAKED_LUA_VER),$(2))
    LUA$(1)_PKG := lua
  endif
endif

ifneq ($$(LUA$(1)_PKG),NONE)
  $$(info lua-scrypt: Lua $(2) package name on this system is $$(LUA$(1)_PKG))
  LUA$(1)_HAS_MODINST := $$(shell test x`pkg-config --variable INSTALL_LMOD $$(LUA$(1)_PKG)` != x && echo YES)
  LUA$(1)_INC := $$(shell pkg-config --cflags $$(LUA$(1)_PKG))
  LUA$(1)_LIB := $$(shell pkg-config --libs $$(LUA$(1)_PKG))
  ifeq ($$(LUA$(1)_HAS_MODINST),)
    LOCAL := YES
  endif
  ifeq ($$(LOCAL),)
    LUA$(1)_LMOD_INST := $$(shell pkg-config --variable=INSTALL_LMOD $$(LUA$(1)_PKG))
    LUA$(1)_CMOD_INST := $$(shell pkg-config --variable=INSTALL_CMOD $$(LUA$(1)_PKG))
  else
    LUA$(1)_LMOD_INST := /usr/local/share/lua/$(2)
    LUA$(1)_CMOD_INST := /usr/local/lib/lua/$(2)
  endif

  .PHONY: lua-$(2)-try
  lua-$(2)-try: lua-$(2)

  .PHONY: lua-$(2)-try-install
  lua-$(2)-try-install: lua-$(2)-install
else
.PHONY: lua-$(2)-try
lua-$(2)-try:
	@echo lua-scrypt: Lua $(2) could not be found, so lua-scrypt was not built for it.

.PHONY: lua-$(2)-try-install
lua-$(2)-try-install:
	@echo lua-scrypt: Lua $(2) could not be found, so lua-scrypt was not installed for it.
endif
endef

$(eval $(call find_lua_libs,51,5.1))
$(eval $(call find_lua_libs,52,5.2))
