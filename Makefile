LIBCRYPT_C := lib/crypto/crypto_aesctr.c \
			   lib/crypto/crypto_scrypt-nosse.c \
			   lib/crypto/crypto_scrypt-ref.c \
			   lib/crypto/sha256.c

INSTALL := /usr/bin/install
SCRYPT_LIBS := -lscrypt
OS := $(shell uname -s)

override CFLAGS := $(CFLAGS) -O2 -Wall -fPIC

.PHONY: all
all: lua-5.1-try lua-5.2-try

include findlua.mk

.PHONY: clean
clean:
	$(RM) scrypt-5.1.so scrypt-5.2.so scrypt.so
	$(RM) luascrypt.o
	$(RM) -r html

%.o: %.c
	 $(CC) $(CFLAGS) -fPIC $(LUA_INC) -c $< -o $@

scrypt-%.so: luascrypt.c base64.c
ifeq ($(OS),Darwin)
	 $(CC) $(CFLAGS) $(LUA_INC) -dynamiclib -o $@ $^ $(SCRYPT_LIBS) -undefined dynamic_lookup
else
	 $(CC) $(CFLAGS) $(LUA_INC) -shared -o $@ $^ $(SCRYPT_LIBS)
endif

.PHONY: install
install: lua-5.1-try-install lua-5.2-try-install

.PHONY: lua-5.1
lua-5.1: LUA_INC := $(LUA51_INC)
lua-5.1: scrypt-5.1.so
	 ln -s -f scrypt-5.1.so scrypt.so

.PHONY: lua-5.1-install
lua-5.1-install: LUA_INC := $(LUA51_INC)
lua-5.1-install: INST_LIBDIR ?= $(DESTDIR)$(LUA51_CMOD_INST)
lua-5.1-install: lua-5.1
	 $(INSTALL) -d $(INST_LIBDIR)
	 $(INSTALL) -m 755 scrypt-5.1.so $(INST_LIBDIR)/scrypt.so

.PHONY: lua-5.2
lua-5.2: LUA_INC := $(LUA52_INC)
lua-5.2: scrypt-5.2.so
	 ln -s -f scrypt-5.2.so scrypt.so

.PHONY: lua-5.2-install
lua-5.2-install: LUA_INC := $(LUA52_INC)
lua-5.2-install: INST_LIBDIR ?= $(DESTDIR)$(LUA52_CMOD_INST)
lua-5.2-install: lua-5.2
	 $(INSTALL) -d $(INST_LIBDIR)
	 $(INSTALL) -m 755 scrypt-5.2.so $(INST_LIBDIR)/scrypt.so

.PHONY: doc
doc:
	@ldoc .
